package controller

import (
	"database/sql"
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddStudent(w http.ResponseWriter, r *http.Request) {
	if !Verifycookie(w, r) {
		return
	}

	// Place to store the request body data
	var stud model.Student

	// Parse the request body
	decoder := json.NewDecoder(r.Body)

	// Storing the data in stud variable
	err := decoder.Decode(&stud)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json data")
	}

	// Error Handling
	// fmt.Fprintf(w, "add student handler")

	saveErr := stud.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
	} else {
		httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"Message": "student data add"})
	}
}

// helper function to convert std string to int
func getUserId(sid string) (int64, error) {

	stdId, err := strconv.ParseInt(sid, 10, 64)
	return stdId, err
}

func GetStud(w http.ResponseWriter, r *http.Request) {
	// parameter
	ssid := mux.Vars(r)["sid"]
	stdid, _ := getUserId(ssid)

	stud := model.Student{StdId: stdid}

	getErr := stud.Read()
	// Way2
	// if getErr != nil {
	// 	httpResp.RespondWithError(w, http.StatusNotFound, getErr.Error())
	// } else {
	// 	httpResp.RespondWithJSON(w, http.StatusOK, stud)
	// }

	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, stud)
	}
}

func UpdateStud(w http.ResponseWriter, r *http.Request) {
	if !Verifycookie(w, r) {
		return
	}

	old_sid := mux.Vars(r)["sid"]
	old_stdId, _ := getUserId(old_sid)

	var stud model.Student
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&stud)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json")
		return
	}

	UpdateErr := stud.Update(old_stdId)

	if UpdateErr != nil {
		switch UpdateErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "student not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, UpdateErr.Error())
		}
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, stud)
	}
}

func DeleteStud(w http.ResponseWriter, r *http.Request) {
	if !Verifycookie(w, r) {
		return
	}

	sid := mux.Vars(r)["sid"]
	stdId, idErr := getUserId(sid)

	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	s := model.Student{StdId: stdId}
	err := s.Delete()

	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func GetAllStuds(w http.ResponseWriter, r *http.Request) {
	if !Verifycookie(w, r) {
		return
	}

	students, getErr := model.GetAllStudents()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, students)
}
